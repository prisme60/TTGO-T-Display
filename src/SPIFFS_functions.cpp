/*====================================================================================
  This sketch contains support functions for the ESP6266 SPIFFS filing system

  Created by Bodmer 15th Jan 2017
  ==================================================================================*/
 
#include <Arduino.h>
#define FS_NO_GLOBALS
#include <FS.h>
#include <SPIFFS.h>

//====================================================================================
//                 Print a SPIFFS directory list (root directory)
//====================================================================================

void listFiles(void) {
  Serial.println();
  Serial.println("SPIFFS files found:");

  fs::File root = SPIFFS.open("/");
 
  fs::File file = root.openNextFile();
 
  while(file){
 
      Serial.print("FILE: ");
      Serial.println(file.name());
 
      file = root.openNextFile();
  }

  Serial.println();
  delay(1000);
}
//====================================================================================

